var Twitter = require('../../lib/twitter').Twitter;
var datagenerator = require('../../helpers/data_generator.js');
var fs = require('fs');
var chai = require('chai');
var should = chai.should();

describe('Tests for POST /statuses/updates endpoint', function () {
    var twitter;
    var config;

    before(function () {
        config = JSON.parse(fs.readFileSync('./config.json', encoding="ascii"));
        twitter = new Twitter(config.apiCredentials.consumerKey, config.apiCredentials.consumerSecret,
        config.apiCredentials.accessToken, config.apiCredentials.accessTokenSecret, config.apiCredentials.callBackUrl);
    });

     it('should post tweet with no parameters', function (done) {
        var params = {};
        twitter.postTweet(params,
            function (response, body) {
                // check status code.
                response.statusCode.should.be.equal(403);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                var expectedResponse = {
                    errors: [
                        {code: 170, message: 'Missing required parameter: status.'}
                    ]
                }
                jsonBody.should.be.deep.equal(expectedResponse);
                done();
            }
        );
    });

    it('should post tweet with empty status', function (done) {
        var params = {status: ''};
        twitter.postTweet(params,
            function (response, body) {
                // check status code.
                response.statusCode.should.be.equal(403);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                var expectedResponse = {
                    errors: [
                        {code: 170, message: 'Missing required parameter: status.'}
                    ]
                }
                jsonBody.should.be.deep.equal(expectedResponse);
                done();
            }
        );
    });

    it('should post tweet with status longer than 140 chars', function (done) {
        var params = {status: datagenerator.randomString(141)};
        twitter.postTweet(params,
            function (response, body) {
                // check status code.
                response.statusCode.should.be.equal(403);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                var expectedResponse = {
                    errors: [
                        {code: 186, message: 'Status is over 140 characters.'}
                    ]
                }
                jsonBody.should.be.deep.equal(expectedResponse);
                done();
            }
        );
    });

    it('should post tweet with required fields only', function (done) {
        var tweetStatus = datagenerator.randomString(20);
        var params = { status: tweetStatus };
        twitter.postTweet(params,
            function (response, body) {
                // check status code
                response.statusCode.should.be.equal(200);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                jsonBody.text.should.be.equal(tweetStatus);

                done();
            }
        );
    });

    it('should post tweet with invalid lat and long coordinates', function (done) {
        var tweetStatus = datagenerator.randomString(20);
        var params = {
            status: tweetStatus,
            lat: 100000,
            long: -100000
        };
        twitter.postTweet(params,
            function (response, body) {
                // check status code
                response.statusCode.should.be.equal(400);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                var expectedResponse = {
                    errors: [
                        {code: 3, message: 'Invalid coordinates.'}
                    ]
                }
                jsonBody.should.be.deep.equal(expectedResponse);
                done();
            }
        );
    });

    it('should post tweet with valid lat and long coordinates', function (done) {
        var params = {
            status: datagenerator.randomString(20),
            lat: datagenerator.randomInteger(-90, 90),
            long: datagenerator.randomInteger(-180, 180)
        };
        twitter.postTweet(params,
            function (response, body) {
                // check status code
                response.statusCode.should.be.equal(200);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                jsonBody.text.should.be.equal(params.status);
                jsonBody.geo.coordinates.should.deep.equal([params.lat, params.long]);

                done();
            }
        );
    });

    

});