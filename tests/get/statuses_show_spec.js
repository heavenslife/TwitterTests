var Twitter = require('../../lib/twitter').Twitter;
var datagenerator = require('../../helpers/data_generator.js');
var fs = require('fs');
var chai = require('chai');
var should = chai.should();

describe('Tests for GET /statuses/show endpoint', function () {
    var twitter;
    var config;

    before(function () {
        config = JSON.parse(fs.readFileSync('./config.json', encoding="ascii"));
        twitter = new Twitter(config.apiCredentials.consumerKey, config.apiCredentials.consumerSecret,
        config.apiCredentials.accessToken, config.apiCredentials.accessTokenSecret, config.apiCredentials.callBackUrl);
    });

    it('should get tweet with no parameters', function (done) {
        var params = {};
        twitter.getTweet(params,
            function (response, body) {
                // check status code.
                response.statusCode.should.be.equal(404);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                var expectedResponse = {
                    errors: [
                        {code: 144, message: 'No status found with that ID.'}
                    ]
                }
                jsonBody.should.be.deep.equal(expectedResponse);
                done();
            }
        );
    });

    it('should get tweet with invalid id', function (done) {
        var params = {id: 'invalidID'};
        twitter.getTweet(params,
            function (response, body) {
                // check status code.
                response.statusCode.should.be.equal(404);

                // check the response body
                body.should.be.json;
                var jsonBody = JSON.parse(body);
                var expectedResponse = {
                    errors: [
                        {code: 144, message: 'No status found with that ID.'}
                    ]
                }
                jsonBody.should.be.deep.equal(expectedResponse);
                done();
            }
        );
    });

    it('should get tweet with valid id', function (done) {

        var tweetStatus = datagenerator.randomString(20);
        var paramOne = { status: tweetStatus };

        twitter.postTweet(paramOne,
            function (response, body) {
                // check status code
                response.statusCode.should.equal(200);
                
                var tweetId = JSON.parse(body).id_str;
                var params = {id: tweetId};

                twitter.getTweet(params,
                    function (resp, bd) {
                        // check status code.
                        resp.statusCode.should.be.equal(200);

                        // check the response body
                        bd.should.be.json;
                        var jsonBody = JSON.parse(bd);
                        jsonBody.id_str.should.equal(tweetId);
                        jsonBody.text.should.equal(tweetStatus);
                        done();
                    });

            });
    });
});