var fs = require('fs');

var config = JSON.parse(fs.readFileSync('./config.json', encoding="ascii"));
var verbosity = config.logVerbosity;

module.exports = {
    logResponse:function(response) {
        if (vebosity == 1 || verbosity == 2) {
            console.log('------Response------');
            console.log('Status Code: ' + response.statusCode);
        }
        if (verbosity == 2) {
            console.log('Content: \n' + response);
        }
    },

    logMessage:function(message) {
        if (vebosity == 1 || verbosity == 2) {
            console.log(message);
        }
    }
}