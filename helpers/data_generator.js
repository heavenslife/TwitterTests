var randomstring = require('randomstring');

module.exports = {
    
    randomString:function(length) {
        return randomstring.generate(length);
    },

    randomInteger:function(low, high) {
        return Math.floor(Math.random() * (high - low + 1) + low);
    }


}